#[derive(Debug, PartialEq, Eq)]
pub enum Token<'input> {
    LParen,
    RParen,
    Sym(&'input str),
    Int(u32),
    Bool(bool)
}

#[derive(Debug, PartialEq, Eq)]
pub struct Span {
    pub start_line: usize,
    pub start_col: usize,
    pub end_line: usize,
    pub end_col: usize
}

// Lexes a single token in the sexpression language.
// Returns None when all input is consumed with no tokens
// Returns Some(Err(...)) if there is no valid token to start the string
// Returns Some(Ok((tok,span,str))) indicating the first token, the span that that token took up, and the remaining tail of the string being lexed
fn lex_one<'input>(str: &'input str, mut start_line: usize, mut start_col: usize) -> Option<Result<(Token<'input>, Span, &'input str), String>> {
    let mut chars = str.char_indices().peekable();
    let mut cur_col = start_col;


    loop { 
	match chars.next() {
	    None => {
		return None
	    }
	    Some((i, c)) => {
		cur_col += 1;
		match c {
		    '(' => {
			return Some(Ok((Token::LParen, Span { start_line, start_col, end_line: start_line + 1, end_col: cur_col }, &str[i+1..])))
		    },
		    ')' => {
			return Some(Ok((Token::RParen, Span { start_line, start_col, end_line: start_line + 1, end_col: cur_col }, &str[i+1..])))
		    },
		    '\t'|'\r'|' ' => {
			start_col += 1;
			continue;
		    },
		    '\n' => {
			start_line += 1;
			start_col = 0;
			continue;
		    }
		    _ => {
			match c.to_digit(10) {
			    Some(mut n) => { // now lexing a number, consume the remaining numbers
				let mut end_ix  = i + 1;
				let mut end_col = start_col + 1;
				loop {
				    match chars.peek() {
					Some((_,c)) => {
					    match c.to_digit(10) {
						Some(d) => {
						    chars.next();
						    n = n * 10 + d;
						    end_ix  += 1;
						    end_col += 1;
						    continue;
						}
						None => { }
					    }
					}
					None => { }
				    }
				    // If we reached here we are done
				    return Some(Ok((Token::Int(n),Span{ start_line, start_col, end_col, end_line: start_line + 1 },&str[end_ix..])))
				}
			    }
			    None => {
				if c.is_alphabetic() {
				    let mut end_ix  = i + 1;
				    let mut end_col = start_col + 1;
				    loop {
					match chars.peek() {
					    Some((_,c)) => {
						if c.is_alphabetic() {
						    chars.next();
						    end_ix  += 1;
						    end_col += 1;
						    continue;
						}
					    }
					    None => { }
					}
					// If we reached here we are done, return the current lexeme/token
					let sym = &str[i..end_ix];
					let span = Span{ start_line, start_col, end_col, end_line: start_line + 1 };
					let tail = &str[end_ix..];
					let tok = match sym {
					    "true" => Token::Bool(true),
					    "false" => Token::Bool(false),
					    _ => Token::Sym(sym),
					};
					return Some(Ok((tok,span,tail)))
				    }
				} else {
				    return Some(Err(format!("lexer error: expected a number, symbol, bool or paren but got {}", c)));
				}
			    }
			}
		    }
		};
	    }
	}
    }
}

pub fn tokenize(mut inp: &str) -> Result<Vec<(Token, Span)>, String> {
    let mut toks = Vec::new();
    let mut cur_line = 0;
    let mut cur_col  = 0;
    loop {
	match lex_one(inp, cur_line, cur_col) {
	    None => {
		return Ok(toks);
	    }
	    Some(Ok((tok, span, rest))) => {
		inp = rest;
		cur_line = span.end_line - 1;
		cur_col  = span.end_col;
		toks.push((tok, span));
	    }
	    Some(Err(e)) => {
		return Err(e);
	    }
	}
    }
}

