use crate::tokenize::{Token, Span};
use crate::tokenize::tokenize;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Atom {
    Sym(String),
    Int(u32),
    Bool(bool)
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Sexp<T> {
    Atom(Atom, T),
    Nest(Vec<Sexp<T>>, T)
}

// EXERCISE: Implement parse_tokens
pub fn parse_tokens(tokens: &[(Token, Span)]) -> Result<Vec<Sexp<Span>>, String> {
    panic!("NYI")
}

// EXERCISE: Implement parse (this one is easy)
pub fn parse(input: &str) -> Result<Vec<Sexp<Span>>, String> {
    panic!("NYI: parse")
}

#[cfg(test)]
mod parse_tests {
    // Remember to test your functions
    // Since the two are so similar, it only seems necessary to unit test one of parse_tokens, parse
}
